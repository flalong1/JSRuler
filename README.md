## JSRuler介绍

JSRuler采用javascript实现，用来在网页中显示标尺。
![JSRuler运行效果图](README/demo.jpg)

## JSRuler依赖
JSRuler依赖jquery，jquery.event.drag和jquery.event.drop

## JSRuler使用

引入文件
```html
<link rel="stylesheet" href="../src/ruler.css" />
<script src="../src/ruler.js"></script>
```

代码示例
```javascript
<script type="text/javascript">
    new Ruler().wrap('rulerInner');
</script>
```